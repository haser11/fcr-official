package com.fallencraftmc.Main.World;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;
import org.bukkit.scheduler.BukkitRunnable;

import com.fallencraftmc.Main.BasicInfo;
import com.fallencraftmc.Main.Config.ConfigManager;

public class LeapTimer extends BukkitRunnable
{
	Plugin plugin;
	boolean hasJumped = false;
	ConfigManager cm = new ConfigManager(plugin);
	Player p;
	
	public LeapTimer(Plugin p)
	{
		this.plugin = p;
	}
	public void setPlayer(Player p)
	{
		this.p = p;
	}
	
	@Override
	public void run() 
	{
		if(cm.getBoolean(p, BasicInfo.HASLEAPED))
		{
			cm.setBoolean(p, BasicInfo.HASLEAPED, false);
			p.sendMessage(ChatColor.BLUE + "[Warrior] " + ChatColor.GREEN + "You can now use LEAP");
		}
	}

}
