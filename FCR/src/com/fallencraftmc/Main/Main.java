package com.fallencraftmc.Main;

import java.util.logging.Level;

import org.bukkit.Bukkit;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

import com.fallencraftmc.Main.Config.ConfigManager;
import com.fallencraftmc.Main.Items.BlackSmithListeners;
import com.fallencraftmc.Main.Items.ItemFunctionality;
import com.fallencraftmc.Main.Monsters.MobSpawnHandler;
import com.fallencraftmc.Main.Players.CommandManager;
import com.fallencraftmc.Main.Players.PlayerClassSystem;
import com.fallencraftmc.Main.Players.PlayerDeathManager;
import com.fallencraftmc.Main.Players.PlayerLoginManager;
import com.fallencraftmc.Main.Players.TimerManager;
import com.fallencraftmc.Main.World.WorldListeners;

//FCR Starting fresh!!
public class Main extends JavaPlugin
{
	ConfigManager cm = new ConfigManager(this);
	public void onEnable()
	{
		//Logs
		Bukkit.getServer().getLogger().log(Level.INFO, BasicInfo.PNAME + " has enabled succefully");
		
		//Event Listeners
		PluginManager manager = getServer().getPluginManager();
		
		manager.registerEvents(new ItemFunctionality(this), this);
		manager.registerEvents(new PlayerLoginManager(this), this);
		manager.registerEvents(new PlayerDeathManager(this), this);
		manager.registerEvents(new TimerManager(this), this);
		manager.registerEvents(new BlackSmithListeners(this), this);
		manager.registerEvents(new MobSpawnHandler(this), this);
		manager.registerEvents(new WorldListeners(this), this);
		
		//Class references 
	    new PlayerClassSystem(this);
	    new TimerManager(this);
	    new CommandManager(this);
	    new ItemFunctionality(this);
	    new ConfigManager(this);
	    new ItemFunctionality(this);
	    new WorldListeners(this);
		
	    ItemFunctionality.craftingAdder();
	    //Command executors
		getCommand("fcr").setExecutor(new CommandManager(this));
		
		//Check server
		cm.checkIfNewServer();
		cm.createUuidRegistry();
		//Adds recipes
	}
	
	public void onDisable()
	{
		cm.saveConfig();
		Bukkit.getServer().getLogger().log(Level.INFO, BasicInfo.PNAME + " has been disabled succefully");
		Bukkit.getScheduler().cancelTasks(this);
	}
	
}
