package com.fallencraftmc.Main.Items;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Damageable;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.entity.Snowball;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.FurnaceRecipe;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.ShapedRecipe;
import org.bukkit.inventory.ShapelessRecipe;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.plugin.Plugin;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.util.Vector;

import com.fallencraftmc.Main.BasicInfo;
import com.fallencraftmc.Main.Config.ConfigManager;
import com.fallencraftmc.Main.Players.DeathTimer;
import com.fallencraftmc.Main.Players.PlayerDeathManager;
import com.fallencraftmc.Main.World.LeapTimer;

public class ItemFunctionality implements Listener
{
	int itemCount;
	int smallItemCount;
	Player robbed;
	Plugin plugin;
	ConfigManager cm = new ConfigManager(plugin);
	public static ItemStack arrowStack = new ItemStack(Material.ARROW, 3);
	public ItemFunctionality(Plugin plugin)
	{
		this.plugin = plugin;
	}
	
	public void healPlayer(Player p)
	{
		cm.setBoolean(p, "Config.isFakeDead", false);
		DeathTimer dm = new DeathTimer(plugin, p);
		
		dm.taskId = 30;
		cm.setBoolean(p, "Config.isFakeDead", false);
		p.setHealth(20.0);
		p.setFoodLevel(20);
		p.removePotionEffect(PotionEffectType.SLOW);
		p.removePotionEffect(PotionEffectType.BLINDNESS);
	}
	
	public static Inventory theifUI = Bukkit.getServer().createInventory(null, 9, "Steal an item");
	
	//Mushroom functionality
	@EventHandler
	public void brownMushroom(PlayerInteractEvent event)
	{
		if(event.getAction() == Action.RIGHT_CLICK_AIR)
		{
			if(event.getPlayer().getItemInHand().getType().equals(Material.BROWN_MUSHROOM))
			{
				if(event.getPlayer().getHealth() <= 14.0)
				{
					//Adds health
					double oldHealth = event.getPlayer().getHealth();
					event.getPlayer().setHealth(oldHealth + 6.0);
					
					//Removes 1 herb from hand
					int oldNum = event.getPlayer().getItemInHand().getAmount();
					ItemStack mushroomStack = new ItemStack(Material.BROWN_MUSHROOM, oldNum - 1);
					
					event.getPlayer().setItemInHand(mushroomStack);
					
					//Plays eating sound
					Bukkit.getPlayer(event.getPlayer().getName()).getWorld().playSound(event.getPlayer().getLocation(), Sound.EAT, 1, 0);

				}
				else if(event.getPlayer().getHealth() >= 14.0 && event.getPlayer().getHealth() < 20.0)
				{
					//Sets health
					event.getPlayer().setHealth(20.0);
					
					//Removes 1 flower from hand
					int oldNum = event.getPlayer().getItemInHand().getAmount();
					ItemStack mushroomStack = new ItemStack(Material.BROWN_MUSHROOM, oldNum - 1);
					
					event.getPlayer().setItemInHand(mushroomStack);
					
					//Plays eating sound
					Bukkit.getPlayer(event.getPlayer().getName()).getWorld().playSound(event.getPlayer().getLocation(), Sound.EAT, 1, 0);

					
					
				}
				else
				{
					event.getPlayer().sendMessage(ChatColor.RED + "You're fully healed!");
				}
				
		   }
		}
	}
	
	
	
	//GOLD HOE FUNCTIONALITY
	@EventHandler
	public void goldHoe(PlayerInteractEntityEvent event)
	{
		if(event.getPlayer().getItemInHand().getType().equals(Material.GOLD_HOE))
		{
		if( cm.getString(event.getPlayer(), "Info.class").equalsIgnoreCase("theif"))
			if(event.getRightClicked() instanceof Player)
			{
				theifUI.clear();
				Player rightClick = (Player) event.getRightClicked();
				robbed = rightClick;
				
				cm.setBoolean(event.getPlayer(), "Config.hasRobbedYet", false);
				
				//Checks the amount of items in the inventory
				for(int i = 0; i < 36; i++)
				{
					if(rightClick.getInventory().getItem(i) != null)
					{
						itemCount++;
					}
				}
				
				//If the inventory is less than 9 then fill remaining spaces with null else fill whole gui
				if(itemCount >= 9)
				{
					for(int i = 0; i < 36; i++)
					{
						if(theifUI.firstEmpty() != -1)
						{
							if(rightClick.getInventory().getItem(i) != null)
							{
								theifUI.addItem(rightClick.getInventory().getItem(i));
							}
						}
					}
				}
				else
				{
					for(int i = 0; i < 36; i++)
					{
						if(rightClick.getInventory().getItem(i) != null)
						{
							smallItemCount++;
							theifUI.addItem(rightClick.getInventory().getItem(i));
						}
					}
				}
				event.getPlayer().openInventory(theifUI);
			}
		}
		else
		{
			
		}
	}
	
	//GOLD HOE CLICK FUNCTIONALITY
	@EventHandler
	public void goldHoeClick(InventoryClickEvent event)
	{
		Player p = (Player) event.getWhoClicked();
			if(event.getInventory().getTitle().equalsIgnoreCase("Steal an item"))
			{
			if(cm.getBoolean(p, "Config.hasRobbedYet") == false)
			{
				if(event.getCurrentItem() != null){
				p.sendMessage(ChatColor.GREEN + "You succefully robbed them!");
				ItemStack item = event.getCurrentItem();
				p.getInventory().addItem(item);
				theifUI.removeItem(item);
				robbed.getInventory().removeItem(item);
				
				cm.setBoolean(p, "Config.hasRobbedYet", true);
				p.closeInventory();
				}
			}
			else
			{
				ItemStack item = event.getCurrentItem();
				p.getInventory().removeItem(item);
				p.sendMessage(ChatColor.RED + "You already robbed today!");
				
				p.closeInventory();
			}
			}
	}
	
	//Medics wood hoe
	@EventHandler
	public void woodHoe(PlayerInteractEntityEvent event)
	{
		if(event.getPlayer().getItemInHand().getType().equals(Material.WOOD_HOE))
		{
			
		if(event.getRightClicked() instanceof Player)
		{
			Player rc = (Player) event.getRightClicked();
			Player p = event.getPlayer();
			
			if(cm.getString(event.getPlayer(), "Info.class").equalsIgnoreCase("medic"))
			{
				if(cm.getBoolean(rc, "Config.isFakeDead") == true)
				{
					cm.setBoolean(rc, "Config.isFakeDead", false);
					this.healPlayer(rc);
					
					p.sendMessage(ChatColor.GREEN + "You have successfully healed " + ChatColor.WHITE + rc.getName());
					rc.sendMessage(ChatColor.WHITE + p.getName() + ChatColor.GREEN + " has healed you.");
				}
				else
				{
					p.sendMessage(ChatColor.RED + "That player isnt dying!");
				}
			}
		}
	}
		else
		{
		
		}
}
	
	@EventHandler
	public void onShoot(PlayerInteractEvent event)
	{
		if(event.getPlayer().getItemInHand().getType().equals(Material.CARROT_STICK))
		{
			Player p = event.getPlayer();
			ItemStack pItem = p.getItemInHand();
			
		if(event.getAction().equals(Action.RIGHT_CLICK_AIR) || event.getAction().equals(Action.RIGHT_CLICK_BLOCK))
		{
			if(pItem.getDurability() <= Material.CARROT_STICK.getMaxDurability())
			{
				p.launchProjectile(Snowball.class);
				p.getItemInHand().setDurability((short) (pItem.getDurability() + 3));
			}
			
			ShapelessRecipe gunRecipe = new ShapelessRecipe(new ItemStack(Material.CARROT_STICK));
			
			gunRecipe.addIngredient(pItem.getData());
			gunRecipe.addIngredient(Material.SNOW_BLOCK);
			gunRecipe.addIngredient(Material.SNOW_BLOCK);
			Bukkit.getServer().addRecipe(gunRecipe);
		}
		}
	}
	
	@EventHandler
	public void onSnowBallHit(EntityDamageByEntityEvent event)
	{
		Entity damager = event.getDamager();

		if(damager instanceof Snowball)
		{
			if(event.getEntity() instanceof Damageable)
			{
				Entity target = (Damageable) event.getEntity();
				double oldHealth = ((Damageable) target).getHealth();
				
				if(target instanceof Player)
				{
					if(oldHealth > 3.0)
					{
						((Player) target).setHealth(oldHealth - 3.0);
					}
					else
					{
						((Player) target).setHealth(0);
					}
				}
				else
				{
					if(oldHealth > 6.0)
					{
						((Damageable) target).setHealth(oldHealth - 6.0);
					}
					else
					{
						((Damageable) target).setHealth(0);
					}
				}
			}
		}
	}
	
	@EventHandler
	public void onThrow(PlayerInteractEvent event)
	{
		if(!event.getPlayer().isOp() && !event.getPlayer().hasPermission(BasicInfo.SNOWBALLPERM))
		{
			if(event.getAction().equals(Action.RIGHT_CLICK_AIR) || event.getAction().equals(Action.RIGHT_CLICK_BLOCK))
			{
				if(event.getPlayer().getItemInHand().getType().equals(Material.SNOW_BALL))
					event.setCancelled(true);
			}
				
		}
	}
	
	public static void craftingAdder()
	{
		//Itemstacks and ItemMetas
		ItemMeta arrowMeta = arrowStack.getItemMeta();
		
		//Lores
		List<String> arrowLore = new ArrayList<String>();
		arrowLore.add(ChatColor.GRAY + "Explodes on impact");
		arrowLore.add(ChatColor.RED + "NOT YET IMPLEMENTED!");
		arrowMeta.setDisplayName(ChatColor.AQUA + "Explosive Arrow");
		arrowMeta.setLore(arrowLore);
		arrowStack.setItemMeta(arrowMeta);
		
		
		ShapedRecipe explosionArrow = new ShapedRecipe(arrowStack);
		explosionArrow.shape("TTT", "TAT", "TTT");
		explosionArrow.setIngredient('T', Material.TNT);
		explosionArrow.setIngredient('A', Material.ARROW);
		
		Bukkit.getServer().addRecipe(explosionArrow);
		
		ItemStack eggStack = new ItemStack(Material.MONSTER_EGG, 1, (byte) 93);
		FurnaceRecipe eggRecipe = new FurnaceRecipe(eggStack, Material.EGG);
		Bukkit.getServer().addRecipe(eggRecipe);
	}
	
	@EventHandler
	public void onRightClick(PlayerInteractEvent event)
	{
		if(event.getAction().equals(Action.RIGHT_CLICK_AIR) || event.getAction().equals(Action.RIGHT_CLICK_BLOCK))
		{
			//Wood sword
			if(event.getPlayer().getItemInHand().getType().equals(Material.WOOD_SWORD) && !cm.getBoolean(event.getPlayer(), BasicInfo.HASLEAPED))
			{
				Player p = event.getPlayer();
				if(cm.getString(p, BasicInfo.CLASSINFO).equalsIgnoreCase("warrior") || p.isOp())
				{
				
				Vector loc = p.getLocation().getDirection();
				Vector vec = new Vector(loc.getX() * 0.8D, 0.8D, loc.getZ() * 0.8D);
				LeapTimer lt = new LeapTimer(plugin);
				
				p.addPotionEffect(new PotionEffect(PotionEffectType.INCREASE_DAMAGE, 80, 0));
				p.addPotionEffect(new PotionEffect(PotionEffectType.SPEED, 80, 0));
				p.addPotionEffect(new PotionEffect(PotionEffectType.DAMAGE_RESISTANCE, 80, 0));
				
				Bukkit.getServer().getWorld(p.getWorld().getName()).playSound(p.getLocation(), Sound.WITHER_SHOOT, 1, 1);
				
				if(!p.isOp())
				{
				lt.setPlayer(p);
				lt.runTaskLater(plugin, 100L);
				cm.setBoolean(p, BasicInfo.HASLEAPED, true);
				}
				
				p.setVelocity(vec);
				}
			}
			else if(event.getPlayer().getItemInHand().getType().equals(Material.STONE_SWORD) && !cm.getBoolean(event.getPlayer(), BasicInfo.HASLEAPED))
			{
				Player p = event.getPlayer();
				if(cm.getString(p, BasicInfo.CLASSINFO).equalsIgnoreCase("warrior") || p.isOp())
				{
				Vector loc = p.getLocation().getDirection();
				Vector vec = new Vector(loc.getX() * 1.0D, 1.0D, loc.getZ() * 1.0D);
				LeapTimer lt = new LeapTimer(plugin);
				
				p.addPotionEffect(new PotionEffect(PotionEffectType.INCREASE_DAMAGE, 100, 0));
				p.addPotionEffect(new PotionEffect(PotionEffectType.SPEED, 100, 0));
				p.addPotionEffect(new PotionEffect(PotionEffectType.DAMAGE_RESISTANCE, 100, 0));
				
				Bukkit.getServer().getWorld(p.getWorld().getName()).playSound(p.getLocation(), Sound.WITHER_SHOOT, 1, 1);
				
				if(!p.isOp())
				{
				lt.setPlayer(p);
				lt.runTaskLater(plugin, 6000L);
				cm.setBoolean(p, BasicInfo.HASLEAPED, true);
				}
				
				p.setVelocity(vec);
				}
			}
			else if(event.getPlayer().getItemInHand().getType().equals(Material.GOLD_SWORD) && !cm.getBoolean(event.getPlayer(), BasicInfo.HASLEAPED))
			{
				Player p = event.getPlayer();
				if(cm.getString(p, BasicInfo.CLASSINFO).equalsIgnoreCase("warrior") || p.isOp())
				{
				Vector loc = p.getLocation().getDirection();
				Vector vec = new Vector(loc.getX() * 1.0D, 1.0D, loc.getZ() * 1.0D);
				LeapTimer lt = new LeapTimer(plugin);
				
				p.addPotionEffect(new PotionEffect(PotionEffectType.INCREASE_DAMAGE, 100, 0));
				p.addPotionEffect(new PotionEffect(PotionEffectType.SPEED, 100, 0));
				p.addPotionEffect(new PotionEffect(PotionEffectType.DAMAGE_RESISTANCE, 100, 0));
				
				Bukkit.getServer().getWorld(p.getWorld().getName()).playSound(p.getLocation(), Sound.WITHER_SHOOT, 1, 1);
				
				if(!p.isOp())
				{
				lt.setPlayer(p);
				lt.runTaskLater(plugin, 6000L);
				cm.setBoolean(p, BasicInfo.HASLEAPED, true);
				}
				
				p.setVelocity(vec);
				}
			}
			else if(event.getPlayer().getItemInHand().getType().equals(Material.IRON_SWORD) && !cm.getBoolean(event.getPlayer(), BasicInfo.HASLEAPED))
			{
				Player p = event.getPlayer();
				if(cm.getString(p, BasicInfo.CLASSINFO).equalsIgnoreCase("warrior") || p.isOp())
				{
				Vector loc = p.getLocation().getDirection();
				Vector vec = new Vector(loc.getX() * 1.1D, 1.1D, loc.getZ() * 1.1D);
				LeapTimer lt = new LeapTimer(plugin);
				
				p.addPotionEffect(new PotionEffect(PotionEffectType.INCREASE_DAMAGE, 100, 0));
				p.addPotionEffect(new PotionEffect(PotionEffectType.SPEED, 100, 0));
				p.addPotionEffect(new PotionEffect(PotionEffectType.DAMAGE_RESISTANCE, 100, 0));
				
				Bukkit.getServer().getWorld(p.getWorld().getName()).playSound(p.getLocation(), Sound.WITHER_SHOOT, 1, 1);
				
				if(!p.isOp())
				{
				lt.setPlayer(p);
				lt.runTaskLater(plugin, 6000L);
				cm.setBoolean(p, BasicInfo.HASLEAPED, true);
				}
				
				p.setVelocity(vec);
				}
			}
			else if(event.getPlayer().getItemInHand().getType().equals(Material.DIAMOND_SWORD) && !cm.getBoolean(event.getPlayer(), BasicInfo.HASLEAPED))
			{ 
				Player p = event.getPlayer();
				if(cm.getString(p, BasicInfo.CLASSINFO).equalsIgnoreCase("warrior") || p.isOp())
				{
				Vector loc = p.getLocation().getDirection();
				Vector vec = new Vector(loc.getX() * 1.2D, 1.2D, loc.getZ() * 1.2D);
				LeapTimer lt = new LeapTimer(plugin);
				
				p.addPotionEffect(new PotionEffect(PotionEffectType.INCREASE_DAMAGE, 100, 0));
				p.addPotionEffect(new PotionEffect(PotionEffectType.SPEED, 100, 0));
				p.addPotionEffect(new PotionEffect(PotionEffectType.DAMAGE_RESISTANCE, 100, 0));
				
				Bukkit.getServer().getWorld(p.getWorld().getName()).playSound(p.getLocation(), Sound.WITHER_SHOOT, 1, 1);
				
				if(!p.isOp())
				{
				lt.setPlayer(p);
				lt.runTaskLater(plugin, 6000L);
				cm.setBoolean(p, BasicInfo.HASLEAPED, true);
				}
				
				p.setVelocity(vec);
				}
			}
		}
	}
	
	//EYE OF ENDER
	@EventHandler
	public void onTalk(AsyncPlayerChatEvent event)
	{
		if(event.getPlayer().getItemInHand().getType().equals(Material.EYE_OF_ENDER))
		{
			if(cm.getString(event.getPlayer(), BasicInfo.CLASSINFO).equalsIgnoreCase("theif") || event.getPlayer().isOp())
			{
				Player[] onlinePlayers = plugin.getServer().getOnlinePlayers();
			    for (Player player : onlinePlayers)
			    {
			  
			    if(cm.getString(player, BasicInfo.CLASSINFO).equalsIgnoreCase("theif") || player.isOp())
			    {
			    	event.setMessage(ChatColor.BLUE + "[Theif] " + ChatColor.WHITE + event.getMessage());
			    }
			    else
			    {
			    	event.setMessage(null);
			    }
			    
			    }
			}
		}
	}
	
	//BOMB
	@EventHandler
	public void onClick(PlayerInteractEvent event)
	{
		if(event.getPlayer().getItemInHand().getType().equals(Material.TNT) && event.getAction().equals(Action.LEFT_CLICK_AIR))
		{
			int oldNum = event.getPlayer().getItemInHand().getAmount();
			Player p = event.getPlayer();
			if(p.getItemInHand().getAmount() > 1)
			{
				event.getPlayer().setItemInHand(new ItemStack(Material.TNT, oldNum - 1));
			}
			else
			{
				p.setItemInHand(null);
			}
			Location pLoc = new Location(p.getWorld(), p.getLocation().getX(), p.getLocation().getY() + 1.0, p.getLocation().getZ());
			
			Entity tnt = Bukkit.getServer().getWorld(event.getPlayer().getWorld().getName()).spawnEntity(pLoc, EntityType.PRIMED_TNT);
			
			Vector loc = event.getPlayer().getLocation().getDirection();
			Vector vec = new Vector(loc.getX() * 0.4D, 0.4D, loc.getZ() * 0.4D);
			
			tnt.setVelocity(vec);
		}
	}
	
}

	
