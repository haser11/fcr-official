package com.fallencraftmc.Main.World;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.LivingEntity;
import org.bukkit.plugin.Plugin;

public class CustomNPC 
{
	Plugin plugin;
	public CustomNPC(Plugin p)
	{
		this.plugin = p;
	}
	public CustomNPC(Location loc, EntityType mobType, String name, boolean display)
	{
		LivingEntity ent = (LivingEntity) Bukkit.getServer().getWorld(loc.getWorld().getName()).spawnEntity(loc, mobType);
		ent.setCustomName(name);
		ent.setCustomNameVisible(display);
	}
}
