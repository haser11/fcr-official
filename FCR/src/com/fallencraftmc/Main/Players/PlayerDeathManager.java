package com.fallencraftmc.Main.Players;

import me.confuser.barapi.BarAPI;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerPickupItemEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.plugin.Plugin;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.scheduler.BukkitRunnable;

import com.fallencraftmc.Main.BasicInfo;
import com.fallencraftmc.Main.Config.ConfigManager;

public class PlayerDeathManager implements Listener 
{
	Plugin plugin;
	
	public PlayerDeathManager(Plugin p)
	{
		this.plugin = p;
	}
	
	TimerManager tm = new TimerManager(plugin);
	 ConfigManager cm = new ConfigManager(plugin);
	
	@EventHandler
	public void onDeath(PlayerDeathEvent event)
	{
		if(event.getEntity() instanceof Player)
		{
			event.setDeathMessage(null);
			Player p = event.getEntity();
			p.setHealth(1.0);
			cm.setBoolean(p, BasicInfo.ISDEAD, true);
			
			if(this.isFakeDead(p) || !p.isOp())
			{
				p.addPotionEffect(new PotionEffect(PotionEffectType.BLINDNESS, 620, 0));
				p.addPotionEffect(new PotionEffect(PotionEffectType.SLOW, 620, 2));
				Bukkit.getServer().getWorld(p.getWorld().getName()).playSound(p.getLocation(), Sound.BLAZE_DEATH, 10, -10);
				p.setFoodLevel(1);
				
				BarAPI.setMessage(p, ChatColor.BOLD.toString() + ChatColor.RED + "You are dying!", 30);
				p.sendMessage(ChatColor.BLUE + "To force kill yourself type /fcr kill");
				tm.deathTimer(p);
			}
		}
	}
	
	
	public boolean isFakeDead(Player p)
	{
		if(cm.getBoolean(p, "Config.isFakeDead"))
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	
	@EventHandler
	public void onDamage(EntityDamageEvent event)
	{
		if(event.getEntity() instanceof Player)
			if(this.isFakeDead((Player) event.getEntity()))
			{
				event.setCancelled(true);
			}
	}
	
	@EventHandler
	public void onPickup(PlayerPickupItemEvent event)
	{
		if(cm.getBoolean(event.getPlayer(), "Config.isFakeDead"))
		{
			event.setCancelled(true);
		}
	}
	
	@EventHandler
	public void onOpenGui(InventoryClickEvent event)
	{
		if(event.getInventory().getName() != event.getWhoClicked().getInventory().getName())
		{
			Player p = (Player) event.getWhoClicked();
			if(cm.getBoolean(p, "Config.isFakeDead"))
			{
				event.setCancelled(true);
				p.closeInventory();
			}
		}
	}
	
	@EventHandler
	public void onBlockBreak(BlockBreakEvent event)
	{
		if(cm.getBoolean(event.getPlayer(), "Config.isFakeDead"))
		{
			event.setCancelled(true);
		}
	}
	
	public void killPlayer(Player p)
	{
		DeathTimer dt = new DeathTimer(plugin, p);
		
		dt.taskId = 30;
		p.removePotionEffect(PotionEffectType.SLOW);
		p.removePotionEffect(PotionEffectType.BLINDNESS);
		p.setHealth(0);
		cm.setBoolean(p, BasicInfo.ISDEAD, false);
		BarAPI.removeBar(p);
		p.teleport(Bukkit.getServer().getWorld(p.getWorld().getName()).getSpawnLocation());
		p.kickPlayer(ChatColor.BOLD.toString() + ChatColor.RED + "YOU HAVE DIED");

	}
	
	@EventHandler
	public void onLogout(PlayerQuitEvent event)
	{
		if(this.isFakeDead(event.getPlayer()))
		{
			cm.setBoolean(event.getPlayer(), BasicInfo.ISDEAD, true);
		}
	}
	
	@EventHandler
	public void onLogin(final PlayerJoinEvent event)
	{
		if(this.isFakeDead(event.getPlayer()))
		{
			Bukkit.getServer().getScheduler().runTaskLater(plugin, new BukkitRunnable()
			{

				@Override
				public void run() 
				{
					event.getPlayer().teleport(Bukkit.getServer().getWorld(BasicInfo.WORLDNAME).getSpawnLocation());
				}
				
			}, 40L);
		
			event.getPlayer().setHealth(20.0);
			event.getPlayer().setFoodLevel(20);
			event.getPlayer().removePotionEffect(PotionEffectType.SLOW);
			event.getPlayer().removePotionEffect(PotionEffectType.BLINDNESS);
			BarAPI.removeBar(event.getPlayer());
			cm.setBoolean(event.getPlayer(), BasicInfo.ISDEAD, false);
			event.getPlayer().sendMessage(ChatColor.RED.toString() + ChatColor.BOLD + "You have died!");
		}
	}
}