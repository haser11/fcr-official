package com.fallencraftmc.Main.Players;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;
import org.bukkit.scheduler.BukkitRunnable;

import com.fallencraftmc.Main.BasicInfo;
import com.fallencraftmc.Main.Config.ConfigManager;

public class DeathTimer extends BukkitRunnable
{
	private Player player;
	public int taskId;
    Plugin plugin;
	public int timer = 30;
	public DeathTimer(Plugin p, Player player)
	{
		this.plugin = p;
		this.player = player;
	}
	public void setPlayer(Player player)
	{
		this.player = player;
	}
	public Player getPlayer()
	{
		return player;
	}
	public void cancelTimer()
	{
	}
	
	ConfigManager cm = new ConfigManager(plugin);
	
	@Override
	public void run() 
	{
		taskId = Bukkit.getScheduler().scheduleSyncRepeatingTask(Bukkit.getServer().getPluginManager().getPlugin("FCR"), new BukkitRunnable()
		{

			@Override
			public void run() 
			{	
				if(timer > 0)
				{
					timer--;
				}
				else if(timer == 0 && cm.getBoolean(player, "Config.isFakeDead"))
				{
					player.teleport(Bukkit.getServer().getWorld("world").getSpawnLocation());
					player.getPlayer().sendMessage(ChatColor.RED + "You have died!");
					player.getPlayer().setHealth(20.0);
					player.getPlayer().setFoodLevel(20);
					player.getPlayer().setLevel(0);
					player.getPlayer().setExp(0);
					timer = 30;
					cm.setBoolean(player, BasicInfo.ISDEAD, false);
					player.kickPlayer(ChatColor.BOLD.toString() + ChatColor.RED + "YOU HAVE DIED");
				}
			}
			
		}, 0L, 20L);
	}

}
