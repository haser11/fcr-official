package com.fallencraftmc.Main;

import org.bukkit.entity.EntityType;

public class FCUtil 
{
	public static EntityType stringToEntityType(String s)
	{
		if(s.equalsIgnoreCase("Zombie"))
		{
			return EntityType.ZOMBIE;
		}
		else if(s.equalsIgnoreCase("Skeleton"))
		{
			return EntityType.SKELETON;
		}
		else if(s.equalsIgnoreCase("Spider"))
		{
			return EntityType.SPIDER;
		}
		else if(s.equalsIgnoreCase("Enderman"))
		{
			return EntityType.ENDERMAN;
		}
		else if(s.equalsIgnoreCase("Creeper"))
		{
			return EntityType.CREEPER;
		}
		else if(s.equalsIgnoreCase("Wolf"))
		{
			return EntityType.WOLF;
		}
		else if(s.equalsIgnoreCase("Ocelot"))
		{
			return EntityType.OCELOT;
		}
		else if(s.equalsIgnoreCase("Villager"))
		{
			return EntityType.VILLAGER;
		}
		else if(s.equalsIgnoreCase("Chicken"))
		{
			return EntityType.CHICKEN;
		}
		else if(s.equalsIgnoreCase("Pig"))
		{
			return EntityType.PIG;
		}
		else if(s.equalsIgnoreCase("Sheep"))
		{
			return EntityType.SHEEP;
		}
		else if(s.equalsIgnoreCase("Cow"))
		{
			return EntityType.COW;
		}
		else if(s.equalsIgnoreCase("Slime"))
		{
			return EntityType.SLIME;
		}
		else if(s.equalsIgnoreCase("Ghast"))
		{
			return EntityType.GHAST;
		}
		else if(s.equalsIgnoreCase("PigZombie"))
		{
			return EntityType.PIG_ZOMBIE;
		}
		else if(s.equalsIgnoreCase("CaveSpider"))
		{
			return EntityType.CAVE_SPIDER;
		}
		else if(s.equalsIgnoreCase("Silverfish"))
		{
			return EntityType.SILVERFISH;
		}
		else if(s.equalsIgnoreCase("Blaze"))
		{
			return EntityType.BLAZE;
		}
		else if(s.equalsIgnoreCase("MagmaCube"))
		{
			return EntityType.MAGMA_CUBE;
		}
		else if(s.equalsIgnoreCase("Bat"))
		{
			return EntityType.BAT;
		}
		else if(s.equalsIgnoreCase("Witch"))
		{
			return EntityType.WITCH;
		}
		else if(s.equalsIgnoreCase("Squid"))
		{
			return EntityType.SQUID;
		}
		else if(s.equalsIgnoreCase("Mooshroom"))
		{
			return EntityType.MUSHROOM_COW;
		}
		else if(s.equalsIgnoreCase("Horse"))
		{
			return EntityType.HORSE;
		}
		else if(s.equalsIgnoreCase("Wither"))
		{
			return EntityType.WITHER;
		}
		else if(s.equalsIgnoreCase("Enderdragon"))
		{
			return EntityType.ENDER_DRAGON;
		}
		
		else
		{
			return EntityType.UNKNOWN;
		}
	}
}
