package com.fallencraftmc.Main.Monsters;

import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Creeper;
import org.bukkit.entity.Enderman;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Skeleton;
import org.bukkit.entity.Spider;
import org.bukkit.entity.Zombie;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.CreatureSpawnEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.Plugin;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

public class MobSpawnHandler implements Listener
{
	Plugin plugin;
	public MobSpawnHandler(Plugin p)
	{
		this.plugin = p;
	}
	
	@EventHandler
	public void onZombieSpawn(CreatureSpawnEvent event)
	{
		if(event.getEntity() instanceof Zombie)
		{
			LivingEntity zom = event.getEntity();
			
			//Sets the zombies armour
			zom.getEquipment().setHelmet(new ItemStack(Material.IRON_HELMET));
			zom.getEquipment().setChestplate(new ItemStack(Material.DIAMOND_CHESTPLATE));
			zom.getEquipment().setLeggings(new ItemStack(Material.IRON_LEGGINGS));
			zom.getEquipment().setBoots(new ItemStack(Material.IRON_BOOTS));
			zom.getEquipment().setItemInHand(new ItemStack(Material.DIAMOND_SWORD));
			
			//Sets the drop chance
			zom.getEquipment().setHelmetDropChance(0.03F);
			zom.getEquipment().setChestplateDropChance(0.01F);
			zom.getEquipment().setLeggingsDropChance(0.02F);
			zom.getEquipment().setBootsDropChance(0.0F);
			zom.getEquipment().setItemInHandDropChance(0.01F);
		}
	}
	
	@EventHandler
	public void onSkeletonSpawn(CreatureSpawnEvent event)
	{
		if(event.getEntity() instanceof Skeleton)
		{
			LivingEntity skele = event.getEntity();
			ItemStack bow = new ItemStack(Material.BOW);
			bow.addEnchantment(Enchantment.ARROW_DAMAGE, 3);
			
			//Sets the skeles armour
			skele.getEquipment().setHelmet(new ItemStack(Material.DIAMOND_HELMET));
			skele.getEquipment().setChestplate(new ItemStack(Material.DIAMOND_CHESTPLATE));
			skele.getEquipment().setBoots(new ItemStack(Material.DIAMOND_BOOTS));
			skele.getEquipment().setItemInHand(bow);
			
			//Sets the drop chance
			skele.getEquipment().setHelmetDropChance(0.03F);
			skele.getEquipment().setChestplateDropChance(0.01F);
			skele.getEquipment().setBootsDropChance(0.01F);
			skele.getEquipment().setItemInHandDropChance(0.01F);
		}
	}
	
	@EventHandler
	public void onSpiderSpawn(CreatureSpawnEvent event)
	{	
		if(event.getEntity() instanceof Spider)
		{
			LivingEntity spidy = event.getEntity();
			ItemStack diamondChest = new ItemStack(Material.DIAMOND_CHESTPLATE);
			diamondChest.addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 2);
			
			//Potion Effects
			spidy.addPotionEffect(new PotionEffect(PotionEffectType.SPEED, 1000000000, 3));
			spidy.addPotionEffect(new PotionEffect(PotionEffectType.INCREASE_DAMAGE, 1000000000, 4));
			spidy.addPotionEffect(new PotionEffect(PotionEffectType.JUMP, 1000000000, 2));
		
			//Armour
			spidy.getEquipment().setChestplate(diamondChest);
			spidy.getEquipment().setChestplateDropChance(0.0F);
		}
	}
	
	@EventHandler
	public void onCreeperSpawn(CreatureSpawnEvent event)
	{
		if(event.getEntity() instanceof Creeper)
		{
			LivingEntity oldCreep = event.getEntity();
			((Creeper) oldCreep).setPowered(true);
		}
	}
	
	@EventHandler
	public void onEndermanSpawn(CreatureSpawnEvent event)
	{
		if(event.getEntity() instanceof Enderman)
		{
			LivingEntity ender = event.getEntity();
			
			//PotionEffects
			ender.addPotionEffect(new PotionEffect(PotionEffectType.INCREASE_DAMAGE, 1000000000, 5));
			ender.addPotionEffect(new PotionEffect(PotionEffectType.DAMAGE_RESISTANCE, 1000000000, 2));
		}
	}
	
}
