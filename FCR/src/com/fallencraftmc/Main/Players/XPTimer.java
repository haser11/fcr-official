package com.fallencraftmc.Main.Players;

import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;
import org.bukkit.scheduler.BukkitRunnable;

import com.fallencraftmc.Main.Config.ConfigManager;

public class XPTimer extends BukkitRunnable
{
	private int timerLength = 30;
	private int id;
	private Player p;
	Plugin plugin;
	
	public int getId()
	{
		return id;
	}
	public void setId(int id)
	{
		this.id = id;
	}
	public void setPlayer(Player player)
	{
		this.p = player;
	}
	public Player getPlayer()
	{
		return p;
	}
	public XPTimer(Plugin p)
	{
		this.plugin = p;
	}
	public void cancelTimer()
	{
		this.cancel();
	}
	
	ConfigManager cm = new ConfigManager(plugin);
	
	@Override
	public void run() 
	{
		if(timerLength > 0 && cm.getBoolean(p, "Config.isFakeDead"))
		{
			this.getPlayer().setLevel(timerLength);
			timerLength--;
		}
		else 
		{
			this.cancel();
			this.getPlayer().setLevel(0);
			this.getPlayer().setExp(0);
		}
	}

}
