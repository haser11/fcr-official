package com.fallencraftmc.Main.Items;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.CraftItemEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.ShapelessRecipe;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.plugin.Plugin;

import com.fallencraftmc.Main.Config.ConfigManager;

public class BlackSmithListeners implements Listener
{
	Plugin plugin;
	ConfigManager cm = new ConfigManager(plugin);
	
	/**
	 * -----------------------------------------------------------------------
	 */
	//ItemStacks swords
	ItemStack stoneSword = new ItemStack(Material.STONE_SWORD);
	ItemStack goldSword = new ItemStack(Material.GOLD_SWORD);
	ItemStack ironSword = new ItemStack(Material.IRON_SWORD);
	ItemStack diamondSword = new ItemStack(Material.DIAMOND_SWORD);
	
	//Itemstacks picks
	ItemStack stonePick = new ItemStack(Material.STONE_PICKAXE);
	ItemStack goldPick = new ItemStack(Material.GOLD_PICKAXE);
	ItemStack ironPick = new ItemStack(Material.IRON_PICKAXE);
	ItemStack diamondPick = new ItemStack(Material.DIAMOND_PICKAXE);
	
	//Itemstacks axes
	ItemStack stoneAxe = new ItemStack(Material.STONE_AXE);
	ItemStack goldAxe = new ItemStack(Material.GOLD_AXE);
	ItemStack ironAxe = new ItemStack(Material.IRON_AXE);
	ItemStack diamondAxe = new ItemStack(Material.DIAMOND_AXE);
	
	//Itemstacks shovels
	ItemStack stoneShovel = new ItemStack(Material.STONE_SPADE);
	ItemStack goldShovel = new ItemStack(Material.GOLD_SPADE);
	ItemStack ironShovel = new ItemStack(Material.IRON_SPADE);
	ItemStack diamondShovel = new ItemStack(Material.DIAMOND_SPADE);
	
	//ItemStacks hoes
	ItemStack stoneHoe = new ItemStack(Material.STONE_HOE);
	ItemStack goldHoe = new ItemStack(Material.GOLD_HOE);
	ItemStack ironHoe = new ItemStack(Material.IRON_HOE);
	ItemStack diamondHoe = new ItemStack(Material.DIAMOND_HOE);
	/**
	 * -----------------------------------------------------------------------------
	 */
	
	List<String> hotLore = new ArrayList<String>();
	
	public BlackSmithListeners(Plugin p)
	{
		this.plugin = p;
	}

	@EventHandler
	public void onCraft(CraftItemEvent event)
	{
		if(event.getWhoClicked() instanceof Player)
		{
			Player p = (Player) event.getWhoClicked();
			
			/**
			 * 
			 * 
			 * WEAPONS
			 * 
			 * 
			 */
			//WOOD SWORD
			if(event.getRecipe().getResult().getType() == Material.WOOD_SWORD)
			{
				if(cm.getString(p, "Info.class").equalsIgnoreCase("blacksmith"))
				{
					p.sendMessage(ChatColor.BLUE + "[BlackSmith]" + ChatColor.GREEN + " Successfully created WOOD_SWORD");
				}
				else
				{
					event.setCancelled(true);
					p.closeInventory();
					p.sendMessage(ChatColor.BLUE + "[BlackSmith]" + ChatColor.RED + " You are not a blacksmith!");
				}
			}
			
			//STONE SWORD
			if(event.getRecipe().getResult().getType() == Material.STONE_SWORD)
			{
				if(cm.getString(p, "Info.class").equalsIgnoreCase("blacksmith"))
				{
					event.getCurrentItem().setDurability((short) (Material.STONE_SWORD.getMaxDurability() + Material.STONE_SWORD.getMaxDurability() - 1));
					p.sendMessage(ChatColor.BLUE + "[BlackSmith]" + ChatColor.GREEN + " Successfully crafted STONE_SWORD");
					
					
					ItemStack playerSword = event.getCurrentItem();
					ItemMeta playerSwordMeta = playerSword.getItemMeta();
					List<String> lore = new ArrayList<String>();
					lore.add(ChatColor.RED + "HOT");
					playerSwordMeta.setLore(lore);
					
					ShapelessRecipe recipe = new ShapelessRecipe(playerSword);
					recipe.addIngredient(Material.LAVA_BUCKET);
					recipe.addIngredient(event.getCurrentItem().getData());
					Bukkit.getServer().addRecipe(recipe);
				}
				else
				{
					event.setCancelled(true);
					p.closeInventory();
					p.sendMessage(ChatColor.BLUE + "[BlackSmith]" + ChatColor.RED + " You are not a blacksmith!");
				}
			}
			
			//IRON SWORD
			if(event.getRecipe().getResult().getType() == Material.IRON_SWORD)
			{
				if(cm.getString(p, "Info.class").equalsIgnoreCase("blacksmith"))
				{
					event.getCurrentItem().setDurability((short) (Material.IRON_SWORD.getMaxDurability() + Material.IRON_SWORD.getMaxDurability() - 1));
					p.sendMessage(ChatColor.BLUE + "[BlackSmith]" + ChatColor.GREEN + " Successfully crafted IRON_SWORD");
				}
				else
				{
					event.setCancelled(true);
					p.closeInventory();
					p.sendMessage(ChatColor.BLUE + "[BlackSmith]" + ChatColor.RED + " You are not a blacksmith!");
				}
			}
			
			//GOLD SWORD
			if(event.getRecipe().getResult().getType() == Material.GOLD_SWORD)
			{
				if(cm.getString(p, "Info.class").equalsIgnoreCase("blacksmith"))
				{
					event.getCurrentItem().setDurability((short) (Material.GOLD_SWORD.getMaxDurability() + Material.GOLD_SWORD.getMaxDurability() - 1));
					p.sendMessage(ChatColor.BLUE + "[BlackSmith]" + ChatColor.GREEN + " Successfully crafted GOLD_SWORD!");
				}
				else
				{
					event.setCancelled(true);
					p.closeInventory();
					p.sendMessage(ChatColor.BLUE + "[BlackSmith]" + ChatColor.RED + " You are not a blacksmith!");
				}
			}
			
			//DIAMOND_SWORD
			if(event.getRecipe().getResult().getType() == Material.DIAMOND_SWORD)
			{
				if(cm.getString(p, "Info.class").equalsIgnoreCase("blacksmith"))
				{
					event.getCurrentItem().setDurability((short) (Material.DIAMOND_SWORD.getMaxDurability() + Material.DIAMOND_SWORD.getMaxDurability() - 1));
					p.sendMessage(ChatColor.BLUE + "[BlackSmith]" + ChatColor.GREEN + " Successfully crafted DIAMOND_SWORD");
				}
				else
				{
					event.setCancelled(true);
					p.closeInventory();
					p.sendMessage(ChatColor.BLUE + "[BlackSmith]" + ChatColor.RED + " You are not a blacksmith!");
				}
			}
			
			//BOW
			if(event.getRecipe().getResult().getType() == Material.BOW)
			{
				if(cm.getString(p, "Info.class").equalsIgnoreCase("blacksmith"))
				{
					p.sendMessage(ChatColor.BLUE + "[BlackSmith]" + ChatColor.GREEN + " Successfully created BOW");
				}
				else
				{
					event.setCancelled(true);
					p.closeInventory();
					p.sendMessage(ChatColor.BLUE + "[BlackSmith]" + ChatColor.RED + " You are not a blacksmith!");
				}
			}
	       
			/**
			 * 
			 * 
			 * PICKAXES
			 * 
			 * 
			 */
			
			//WOOD PICK
			if(event.getRecipe().getResult().getType() == Material.WOOD_PICKAXE)
			{
				if(cm.getString(p, "Info.class").equalsIgnoreCase("blacksmith"))
				{
					p.sendMessage(ChatColor.BLUE + "[BlackSmith]" + ChatColor.GREEN + " Successfully created WOOD_PICKAXE");
				}
				else
				{
					event.setCancelled(true);
					p.closeInventory();
					p.sendMessage(ChatColor.BLUE + "[BlackSmith]" + ChatColor.RED + " You are not a blacksmith!");
				}
			}
			
			//STONE PICK
			if(event.getRecipe().getResult().getType() == Material.STONE_PICKAXE)
			{
				if(cm.getString(p, "Info.class").equalsIgnoreCase("blacksmith"))
				{
					p.sendMessage(ChatColor.BLUE + "[BlackSmith]" + ChatColor.GREEN + " Successfully created STONE_PICKAXE");
					event.getCurrentItem().setDurability((short) (Material.STONE_PICKAXE.getMaxDurability() + Material.STONE_PICKAXE.getMaxDurability() - 1));
				}
				else
				{
					event.setCancelled(true);
					p.closeInventory();
					p.sendMessage(ChatColor.BLUE + "[BlackSmith]" + ChatColor.RED + " You are not a blacksmith!");
				}
			}
			
			//GOLD PICK
			if(event.getRecipe().getResult().getType() == Material.GOLD_PICKAXE)
			{
				if(cm.getString(p, "Info.class").equalsIgnoreCase("blacksmith"))
				{
					p.sendMessage(ChatColor.BLUE + "[BlackSmith]" + ChatColor.GREEN + " Successfully created GOLD_PICKAXE");
					event.getCurrentItem().setDurability((short) (Material.GOLD_PICKAXE.getMaxDurability() + Material.GOLD_PICKAXE.getMaxDurability() - 1));
				}
				else
				{
					event.setCancelled(true);
					p.closeInventory();
					p.sendMessage(ChatColor.BLUE + "[BlackSmith]" + ChatColor.RED + " You are not a blacksmith!");
				}
			}
			
			//IRON PICK
			if(event.getRecipe().getResult().getType() == Material.IRON_PICKAXE)
			{
				if(cm.getString(p, "Info.class").equalsIgnoreCase("blacksmith"))
				{
					p.sendMessage(ChatColor.BLUE + "[BlackSmith]" + ChatColor.GREEN + " Successfully created IRON_PICKAXE");
					event.getCurrentItem().setDurability((short) (Material.IRON_PICKAXE.getMaxDurability() + Material.IRON_PICKAXE.getMaxDurability() - 1));
				}
				else
				{
					event.setCancelled(true);
					p.closeInventory();
					p.sendMessage(ChatColor.BLUE + "[BlackSmith]" + ChatColor.RED + " You are not a blacksmith!");
				}
			}
			
			//DIAMOND PICK
			if(event.getRecipe().getResult().getType() == Material.DIAMOND_PICKAXE)
			{
				if(cm.getString(p, "Info.class").equalsIgnoreCase("blacksmith"))
				{
					p.sendMessage(ChatColor.BLUE + "[BlackSmith]" + ChatColor.GREEN + " Successfully created DIAMOND_PICKAXE");
					event.getCurrentItem().setDurability((short) (Material.DIAMOND_PICKAXE.getMaxDurability() + Material.DIAMOND_PICKAXE.getMaxDurability() - 1));
				}
				else
				{
					event.setCancelled(true);
					p.closeInventory();
					p.sendMessage(ChatColor.BLUE + "[BlackSmith]" + ChatColor.RED + " You are not a blacksmith!");
				}
			}
			
			
			/**
			 * 
			 * 
			 * AXES
			 * 
			 * 
			 */
			
			//WOOD AXE
			if(event.getRecipe().getResult().getType() == Material.WOOD_AXE)
			{
				if(cm.getString(p, "Info.class").equalsIgnoreCase("blacksmith"))
				{
					p.sendMessage(ChatColor.BLUE + "[BlackSmith]" + ChatColor.GREEN + " Successfully created WOOD_AXE");
				}
				else
				{
					event.setCancelled(true);
					p.closeInventory();
					p.sendMessage(ChatColor.BLUE + "[BlackSmith]" + ChatColor.RED + " You are not a blacksmith!");
				}
			}
			
			//STONE AXE
			if(event.getRecipe().getResult().getType() == Material.STONE_AXE)
			{
				if(cm.getString(p, "Info.class").equalsIgnoreCase("blacksmith"))
				{
					p.sendMessage(ChatColor.BLUE + "[BlackSmith]" + ChatColor.GREEN + " Successfully created STONE_AXE");
					event.getCurrentItem().setDurability((short) (Material.STONE_AXE.getMaxDurability() + Material.STONE_AXE.getMaxDurability() - 1));
				}
				else
				{
					event.setCancelled(true);
					p.closeInventory();
					p.sendMessage(ChatColor.BLUE + "[BlackSmith]" + ChatColor.RED + " You are not a blacksmith!");
				}
			}
			
			//GOLD AXE
			if(event.getRecipe().getResult().getType() == Material.GOLD_AXE)
			{
				if(cm.getString(p, "Info.class").equalsIgnoreCase("blacksmith"))
				{
					p.sendMessage(ChatColor.BLUE + "[BlackSmith]" + ChatColor.GREEN + " Successfully created GOLD_AXE");
					event.getCurrentItem().setDurability((short) (Material.GOLD_AXE.getMaxDurability() + Material.GOLD_AXE.getMaxDurability() - 1));
				}
				else
				{
					event.setCancelled(true);
					p.closeInventory();
					p.sendMessage(ChatColor.BLUE + "[BlackSmith]" + ChatColor.RED + " You are not a blacksmith!");
				}
			}
			
			//IRON AXE
			if(event.getRecipe().getResult().getType() == Material.IRON_AXE)
			{
				if(cm.getString(p, "Info.class").equalsIgnoreCase("blacksmith"))
				{
					p.sendMessage(ChatColor.BLUE + "[BlackSmith]" + ChatColor.GREEN + " Successfully created IRON_AXE");
					event.getCurrentItem().setDurability((short) (Material.IRON_AXE.getMaxDurability() + Material.IRON_AXE.getMaxDurability() - 1));
				}
				else
				{
					event.setCancelled(true);
					p.closeInventory();
					p.sendMessage(ChatColor.BLUE + "[BlackSmith]" + ChatColor.RED + " You are not a blacksmith!");
				}
			}
			
			//DIAMOND AXE
			if(event.getRecipe().getResult().getType() == Material.DIAMOND_AXE)
			{
				if(cm.getString(p, "Info.class").equalsIgnoreCase("blacksmith"))
				{
					p.sendMessage(ChatColor.BLUE + "[BlackSmith]" + ChatColor.GREEN + " Successfully created DIAMOND_AXE");
					event.getCurrentItem().setDurability((short) (Material.DIAMOND_AXE.getMaxDurability() + Material.DIAMOND_AXE.getMaxDurability() - 1));
				}
				else
				{
					event.setCancelled(true);
					p.closeInventory();
					p.sendMessage(ChatColor.BLUE + "[BlackSmith]" + ChatColor.RED + " You are not a blacksmith!");
				}
			}
			
			/**
			 * 
			 * 
			 * SHOVELS
			 * 
			 * 
			 */
			
			//WOOD SHOVEL
			if(event.getRecipe().getResult().getType() == Material.WOOD_SPADE)
			{
				if(cm.getString(p, "Info.class").equalsIgnoreCase("blacksmith"))
				{
					p.sendMessage(ChatColor.BLUE + "[BlackSmith]" + ChatColor.GREEN + " Successfully created WOOD_SPADE");
				}
				else
				{
					event.setCancelled(true);
					p.closeInventory();
					p.sendMessage(ChatColor.BLUE + "[BlackSmith]" + ChatColor.RED + " You are not a blacksmith!");
				}
			}
			
			//STONE SHOVEL
			if(event.getRecipe().getResult().getType() == Material.STONE_SPADE)
			{
				if(cm.getString(p, "Info.class").equalsIgnoreCase("blacksmith"))
				{
					p.sendMessage(ChatColor.BLUE + "[BlackSmith]" + ChatColor.GREEN + " Successfully created STONE_SPADE");
					event.getCurrentItem().setDurability((short) (Material.STONE_SPADE.getMaxDurability() + Material.STONE_SPADE.getMaxDurability() - 1));
				}
				else
				{
					event.setCancelled(true);
					p.closeInventory();
					p.sendMessage(ChatColor.BLUE + "[BlackSmith]" + ChatColor.RED + " You are not a blacksmith!");
				}
			}
			
			//GOLD SHOVEL
			if(event.getRecipe().getResult().getType() == Material.GOLD_SPADE)
			{
				if(cm.getString(p, "Info.class").equalsIgnoreCase("blacksmith"))
				{
					p.sendMessage(ChatColor.BLUE + "[BlackSmith]" + ChatColor.GREEN + " Successfully created GOLD_SPADE");
					event.getCurrentItem().setDurability((short) (Material.GOLD_SPADE.getMaxDurability() + Material.GOLD_SPADE.getMaxDurability() - 1));
				}
				else
				{
					event.setCancelled(true);
					p.closeInventory();
					p.sendMessage(ChatColor.BLUE + "[BlackSmith]" + ChatColor.RED + " You are not a blacksmith!");
				}
			}
			
			//IRON SHOVEL
			if(event.getRecipe().getResult().getType() == Material.IRON_SPADE)
			{
				if(cm.getString(p, "Info.class").equalsIgnoreCase("blacksmith"))
				{
					p.sendMessage(ChatColor.BLUE + "[BlackSmith]" + ChatColor.GREEN + " Successfully created IRON_SPADE");
					event.getCurrentItem().setDurability((short) (Material.IRON_SPADE.getMaxDurability() + Material.IRON_SPADE.getMaxDurability() - 1));
				}
				else
				{
					event.setCancelled(true);
					p.closeInventory();
					p.sendMessage(ChatColor.BLUE + "[BlackSmith]" + ChatColor.RED + " You are not a blacksmith!");
				}
			}
			
			//DIAMOND SHOVEL
			if(event.getRecipe().getResult().getType() == Material.DIAMOND_SPADE)
			{
				if(cm.getString(p, "Info.class").equalsIgnoreCase("blacksmith"))
				{
					p.sendMessage(ChatColor.BLUE + "[BlackSmith]" + ChatColor.GREEN + " Successfully created DIAMOND_SPADE");
					event.getCurrentItem().setDurability((short) (Material.DIAMOND_SPADE.getMaxDurability() + Material.DIAMOND_SPADE.getMaxDurability() - 1));
				}
				else
				{
					event.setCancelled(true);
					p.closeInventory();
					p.sendMessage(ChatColor.BLUE + "[BlackSmith]" + ChatColor.RED + " You are not a blacksmith!");
				}
			}
			
			
			/**
			 * 
			 * 
			 * HOES
			 * 
			 * 
			 */
			
			//WOOD HOE
			if(event.getRecipe().getResult().getType() == Material.WOOD_HOE)
			{
				if(cm.getString(p, "Info.class").equalsIgnoreCase("blacksmith"))
				{
					p.sendMessage(ChatColor.BLUE + "[BlackSmith]" + ChatColor.GREEN + " Successfully created WOOD_HOE");
				}
				else
				{
					event.setCancelled(true);
					p.closeInventory();
					p.sendMessage(ChatColor.BLUE + "[BlackSmith]" + ChatColor.RED + " You are not a blacksmith!");
				}
			}
			
			//STONE HOE
			if(event.getRecipe().getResult().getType() == Material.STONE_HOE)
			{
				if(cm.getString(p, "Info.class").equalsIgnoreCase("blacksmith"))
				{
					p.sendMessage(ChatColor.BLUE + "[BlackSmith]" + ChatColor.GREEN + " Successfully created STONE_HOE");
					event.getCurrentItem().setDurability((short) (Material.STONE_HOE.getMaxDurability() + Material.STONE_HOE.getMaxDurability() - 1));
				}
				else
				{
					event.setCancelled(true);
					p.closeInventory();
					p.sendMessage(ChatColor.BLUE + "[BlackSmith]" + ChatColor.RED + " You are not a blacksmith!");
				}
			}
			
			//GOLD HOE
			if(event.getRecipe().getResult().getType() == Material.GOLD_HOE)
			{
				if(cm.getString(p, "Info.class").equalsIgnoreCase("blacksmith"))
				{
					p.sendMessage(ChatColor.BLUE + "[BlackSmith]" + ChatColor.GREEN + " Successfully created GOLD_HOE");
					event.getCurrentItem().setDurability((short) (Material.GOLD_HOE.getMaxDurability() + Material.GOLD_HOE.getMaxDurability() - 1));
				}
				else
				{
					event.setCancelled(true);
					p.closeInventory();
					p.sendMessage(ChatColor.BLUE + "[BlackSmith]" + ChatColor.RED + " You are not a blacksmith!");
				}
			}
			
			//IRON HOE
			if(event.getRecipe().getResult().getType() == Material.IRON_HOE)
			{
				if(cm.getString(p, "Info.class").equalsIgnoreCase("blacksmith"))
				{
					p.sendMessage(ChatColor.BLUE + "[BlackSmith]" + ChatColor.GREEN + " Successfully created IRON_HOE");
					event.getCurrentItem().setDurability((short) (Material.IRON_HOE.getMaxDurability() + Material.IRON_HOE.getMaxDurability() - 1));
				}
				else
				{
					event.setCancelled(true);
					p.closeInventory();
					p.sendMessage(ChatColor.BLUE + "[BlackSmith]" + ChatColor.RED + " You are not a blacksmith!");
				}
			}
			
			//DIAMOND HOE
			if(event.getRecipe().getResult().getType() == Material.DIAMOND_HOE)
			{
				if(cm.getString(p, "Info.class").equalsIgnoreCase("blacksmith"))
				{
					p.sendMessage(ChatColor.BLUE + "[BlackSmith]" + ChatColor.GREEN + " Successfully created DIAMOND_HOE");
					event.getCurrentItem().setDurability((short) (Material.DIAMOND_HOE.getMaxDurability() + Material.DIAMOND_HOE.getMaxDurability() - 1));
				}
				else
				{
					event.setCancelled(true);
					p.closeInventory();
					p.sendMessage(ChatColor.BLUE + "[BlackSmith]" + ChatColor.RED + " You are not a blacksmith!");
				}
			}
		}
	}
}
