package com.fallencraftmc.Main.Players;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.Plugin;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

public class PlayerClassSystem 
{
	Plugin plugin;
	
	//Constructors
	public PlayerClassSystem(Player p, String classname)
	{
		if(classname.equalsIgnoreCase("medic"))
		{
			this.classMedic(p); 
			p.sendMessage(ChatColor.GREEN + "You have become a " + classname);
		}
			
		if(classname.equalsIgnoreCase("soldier"))
		{
			this.classSoldier(p); 
			p.sendMessage(ChatColor.GREEN + "You have become a " + classname);
		}
		
		if(classname.equalsIgnoreCase("scout"))
		{
			this.classScout(p); 
			p.sendMessage(ChatColor.GREEN + "You have become a " + classname);
		}
			
		if(classname.equalsIgnoreCase("theif"))
		{
			this.classTheif(p); 
			p.sendMessage(ChatColor.GREEN + "You have become a " + classname);
		}
			
		if(classname.equalsIgnoreCase("archer"))
		{
			this.classArcher(p); 
			p.sendMessage(ChatColor.GREEN + "You have become an " + classname);
		}
			
		if(classname.equalsIgnoreCase("warrior"))
		{
			this.classWarrior(p); 
			p.sendMessage(ChatColor.GREEN + "You have become a " + classname);
		}
			
		if(classname.equalsIgnoreCase("farmer"))
		{
			this.classFarmer(p); 
			p.sendMessage(ChatColor.GREEN + "You have become a " + classname);
		}
			
		if(classname.equalsIgnoreCase("miner"))
		{
			this.classMiner(p); 
			p.sendMessage(ChatColor.GREEN + "You have become a " + classname);
		}
		
		if(classname.equalsIgnoreCase("blacksmith"))
		{
			this.classBlacksmith(p);
			p.sendMessage(ChatColor.GREEN + "You have become a " + classname);
		}
			
		
	}

	public PlayerClassSystem(Plugin plugin)
	{
		this.plugin = plugin;
	}
	
	//Classes
	private void classMedic(Player p)
	{
		//Itemstacks
		p.getInventory().addItem(new ItemStack(Material.WOOD_HOE, 1));
		p.getInventory().addItem(new ItemStack(Material.BROWN_MUSHROOM, 3));
		p.getInventory().addItem(new ItemStack(Material.WOOD_SWORD, 1));
		p.getInventory().addItem(new ItemStack(Material.BREAD, 15));
		
		
		//Armour
		ItemStack leatherChest = new ItemStack(Material.LEATHER_CHESTPLATE, 1);
		leatherChest.addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 2);
		
		p.getInventory().setChestplate(leatherChest);
	}
	
	private void classSoldier(Player p)
	{
		
		//Itemstacks
		ItemStack sword = new ItemStack(Material.STONE_SWORD, 1);
		sword.addEnchantment(Enchantment.DAMAGE_ALL, 1);
		p.getInventory().addItem(new ItemStack(Material.BREAD, 15));
		p.getInventory().addItem(new ItemStack(Material.FIREWORK, 1));
		p.getInventory().addItem(new ItemStack(Material.CARROT_STICK));
		p.getInventory().addItem(sword);
		
		//Armour
		ItemStack ironHelm = new ItemStack(Material.IRON_HELMET, 1);
		ItemStack leatherChest = new ItemStack(Material.LEATHER_CHESTPLATE, 1);
		leatherChest.addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 3);
		
		p.getInventory().setHelmet(ironHelm);
		p.getInventory().setChestplate(leatherChest);
	}
	
	private void classScout(Player p)
	{
		//ItemStacks
		p.getInventory().addItem(new ItemStack(Material.IRON_SWORD));
		p.getInventory().addItem(new ItemStack(Material.COOKED_BEEF, 15));
		p.getInventory().addItem(new ItemStack(Material.YELLOW_FLOWER, 3));
		p.getInventory().addItem(new ItemStack(Material.GOLDEN_APPLE));
		p.getInventory().addItem(new ItemStack(Material.TNT, 3));
		
		//Armour
		ItemStack leatherChest = new ItemStack(Material.LEATHER_CHESTPLATE);
		leatherChest.addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 3);
		ItemStack leatherLeg = new ItemStack(Material.LEATHER_LEGGINGS);
		leatherLeg.addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 3);
		
		p.getInventory().setChestplate(leatherChest);
		p.getInventory().setLeggings(leatherLeg);
	}
	
	private void classTheif(Player p)
	{
		//ItemStacks
		p.getInventory().addItem(new ItemStack(Material.WOOD_SWORD));
		p.getInventory().addItem(new ItemStack(Material.GOLD_HOE));
		p.getInventory().addItem(new ItemStack(Material.BREAD, 15));
		p.getInventory().addItem(new ItemStack(Material.EYE_OF_ENDER));
		
		//Armour
		ItemStack goldChest = new ItemStack(Material.GOLD_CHESTPLATE);
		goldChest.addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 1);
		goldChest.addEnchantment(Enchantment.DURABILITY, 1);
		
		p.getInventory().setChestplate(goldChest);
	}
	private void classArcher(Player p)
	{
		//ItemStacks
		ItemStack bow = new ItemStack(Material.BOW);
		bow.addEnchantment(Enchantment.DURABILITY, 1);
		p.getInventory().addItem(bow);
		p.getInventory().addItem(new ItemStack(Material.ARROW, 32));
		p.getInventory().addItem(new ItemStack(Material.BREAD, 15));
		p.getInventory().addItem(new ItemStack(Material.WOOD_SWORD));
		
		//Effects
		p.addPotionEffect(new PotionEffect(PotionEffectType.SPEED, 1000000000, 0));
		
		//Armour
		ItemStack leatherChest = new ItemStack(Material.LEATHER_CHESTPLATE);
		leatherChest.addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 2);
		
		p.getInventory().setChestplate(leatherChest);
		
		
		
	}
	private void classWarrior(Player p)
	{
		//Itemstacks
		ItemStack stoneSword = new ItemStack(Material.STONE_SWORD);
		stoneSword.addEnchantment(Enchantment.DAMAGE_ALL, 3);
		stoneSword.addEnchantment(Enchantment.DURABILITY, 1);
		
		p.getInventory().addItem(stoneSword);
		p.getInventory().addItem(new ItemStack(Material.COOKED_BEEF, 10));
		p.getInventory().addItem(new ItemStack(Material.FIREWORK));
		//TODO ADD POTION TO PLAYER
		
		//Armour
		ItemStack ironChest = new ItemStack(Material.IRON_CHESTPLATE);
		ironChest.addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 1);
		ItemStack leatherHelm = new ItemStack(Material.LEATHER_HELMET);
		
		p.getInventory().setHelmet(leatherHelm);
		p.getInventory().setChestplate(ironChest);

		
	}
	private void classFarmer(Player p)
	{
		//Itemstacks
		ItemStack ironHoe = new ItemStack(Material.IRON_HOE);
		ironHoe.addEnchantment(Enchantment.DURABILITY, 1);
		
		p.getInventory().addItem(ironHoe);
		p.getInventory().addItem(new ItemStack(Material.WOOD_SWORD));
		p.getInventory().addItem(new ItemStack(Material.BREAD, 15));
		p.getInventory().addItem(new ItemStack(Material.POTATO, 3));
		p.getInventory().addItem(new ItemStack(Material.CARROT, 3));
		p.getInventory().addItem(new ItemStack(Material.SEEDS, 3));
		p.getInventory().addItem(new ItemStack(Material.BONE, 1));
		
		//Armour
		ItemStack leatherChest = new ItemStack(Material.LEATHER_CHESTPLATE);
		leatherChest.addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 1);
	}
	private void classMiner(Player p)
	{
		//Itemstacks
		ItemStack ironPick = new ItemStack(Material.IRON_PICKAXE);
		ironPick.addEnchantment(Enchantment.DURABILITY, 2);
		
		p.getInventory().addItem(ironPick);
		p.getInventory().addItem(new ItemStack(Material.STONE_SWORD));
		p.getInventory().addItem(new ItemStack(Material.TORCH, 32));
		p.getInventory().addItem(new ItemStack(Material.BREAD, 15));
		
		//Armour
		ItemStack ironBoots = new ItemStack(Material.IRON_BOOTS);
		ironBoots.addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 1);
		ironBoots.addEnchantment(Enchantment.DURABILITY, 1);
		ironBoots.addEnchantment(Enchantment.PROTECTION_FALL, 1);
		
		p.getInventory().setBoots(ironBoots);
		
	}
	private void classBlacksmith(Player p)
	{
		//Itemstacks
		ItemStack leatherChest = new ItemStack(Material.LEATHER_CHESTPLATE);
		leatherChest.addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 1);
		
		p.getInventory().addItem(new ItemStack(Material.GOLD_INGOT, 18));
		p.getInventory().addItem(new ItemStack(Material.STONE_SWORD));
		p.getInventory().addItem(new ItemStack(Material.LAVA_BUCKET));
		p.getInventory().addItem(new ItemStack(Material.WATER_BUCKET));
		p.getInventory().addItem(new ItemStack(Material.CAULDRON));
		p.getInventory().addItem(new ItemStack(Material.BREAD, 15));
		
		//Armour
		p.getInventory().setChestplate(leatherChest);
		p.getInventory().setBoots(new ItemStack(Material.GOLD_BOOTS));
	}
	
}
