package com.fallencraftmc.Main;

public class BasicInfo 
{
	public static String PNAME = "FCR";
	public static String PVERSION = "Alpha 4.0.0";
	
	//PERMISSIONS
	public static String MINEPERM = "fcr.world.mine";
	public static String FARMPERM = "fcr.world.farm";
	public static String SETCLASSPERM = "fcr.commands.setclass";
	public static String SAVECONFIGPERM = "fcr.commands.saveconfig";
	public static String LOADCONFIGPERM = "fcr.commands.loadconfig";
	public static String COMEBACKPERM = "fcr.commands.comeback";
	public static String SNOWBALLPERM = "fcr.items.snowball";
	
	//CONFIG ENTRIES
	public static String HASROBBED = "Config.hasRobbedYet";
	public static String ISDEAD = "Config.isFakeDead";
	public static String CLASSINFO = "Info.class";
	public static String HASLEAPED = "Config.hasLeaped";
	
	//COMMANDS
	public static String SAVECONFIG = "saveconfig";
	public static String LOADCONFIG = "loadconfig";
	public static String ENDTIMER = "comeback";
	public static String CLASSLIST = "classlist";
	public static String SPWANMOB = "spawnnpc";
	public static String SETCLASS = "setclass";
	public static String GETUUID = "getuuid";
	public static String GETNAME = "getname";
	
	//MISC
	public static String WORLDNAME = "world";
}
