package com.fallencraftmc.Main.Players;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.enchantment.PrepareItemEnchantEvent;
import org.bukkit.event.player.PlayerRespawnEvent;
import org.bukkit.plugin.Plugin;

import com.fallencraftmc.Main.Config.ConfigManager;

public class TimerManager implements Listener
{
	static Plugin plugin;
	ConfigManager cm = new ConfigManager(plugin);
	public int deathTimer = 30;
	int deathID;
	
	public TimerManager(Plugin p)
	{
		this.plugin = p;
	}
	
	public void deathXPTimer(final Player p)
	{
		XPTimer xt = new XPTimer(this.plugin);
		xt.setPlayer(p);
		xt.runTaskTimer(Bukkit.getServer().getPluginManager().getPlugin("FCR"), 0L, 20L);
	}
	public void deathTimer(Player p)
	{
		DeathTimer dm = new DeathTimer(plugin, p);
		dm.setPlayer(p);
		dm.runTaskLater(Bukkit.getServer().getPluginManager().getPlugin("FCR"), 0L);
	}
	
	@EventHandler
	public void onRespawn(PlayerRespawnEvent event)
	{
		this.deathTimer = 30;
		event.getPlayer().setLevel(0);
	}
	
	@EventHandler
	public void onEnchant(PrepareItemEnchantEvent event)
	{
		if(cm.getBoolean(event.getEnchanter(), "Config.isFakeDead"))
		{
			event.setCancelled(true);
		}
	}
}
