package com.fallencraftmc.Main.Config;

import java.io.File;
import java.io.IOException;
import java.util.logging.Level;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;

import com.fallencraftmc.Main.BasicInfo;


public class ConfigManager
{
	Plugin plugin;
	File dir = new File("plugins" + File.separator + "FCR");
	File usersDir = new File(dir + File.separator + "users" + File.separator);
	
	File uuidEntry = new File(dir + File.separator + "uuids.yml");
	
	FileConfiguration config = null;
	FileConfiguration config2 = null;
	
	public ConfigManager(Plugin p)
	{
		this.plugin = p;
	}
	
	public void checkIfNewServer()
	{
		if(!usersDir.exists())
		{
			try
			{
				Bukkit.getServer().getLogger().log(Level.WARNING, "No config for FCR found! Atempting to create one...");
				usersDir.mkdirs();
				Bukkit.getServer().getLogger().log(Level.INFO, "Successfully created config for FCR!");
			}
			catch(Exception e)
			{
				e.printStackTrace();
				Bukkit.getLogger().log(Level.WARNING, "Uh oh, haser broke something");
			}
		}
	}
	
	public void createUuidRegistry()
	{
		if(!uuidEntry.exists())
		{
			try 
			{
				uuidEntry.createNewFile();
				this.config = YamlConfiguration.loadConfiguration(uuidEntry);
				this.config.get("Players");
				this.config.save(uuidEntry);
			} catch (IOException e) 
			{
				e.printStackTrace();
			}
		}
	}
	
	public void addPlayerToUuidRegistry(Player p)
	{
		this.config = YamlConfiguration.loadConfiguration(uuidEntry);
		this.config.set("Players." + p.getUniqueId(), p.getName());
		try 
		{
			this.config.save(uuidEntry);
		} catch (Exception e) 
		{
			// TODO: handle exception
		}
	}
	
	public boolean checkIfNewPlayer(Player p)
	{
		this.config = YamlConfiguration.loadConfiguration(uuidEntry);
		if(this.config.getString("Players." + p.getUniqueId(), null) == null)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	
	public void createPlayerConfigFile(Player p)
	{
		this.config = YamlConfiguration.loadConfiguration(uuidEntry);
		
			File pFile = new File(dir + File.separator + "users" + File.separator + this.config.getString("Players." + p.getUniqueId()).toString() + ".yml");
			try
			{
			pFile.createNewFile();
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
		}
	
	public void addDefaultPlayerConfig(Player p)
	{
		this.config = YamlConfiguration.loadConfiguration(uuidEntry);
		
		this.setBoolean(p, BasicInfo.HASROBBED, false);
		this.setBoolean(p, BasicInfo.ISDEAD, false);
		this.setBoolean(p, BasicInfo.HASLEAPED, false);
		this.setString(p, BasicInfo.CLASSINFO, "null");

	}
	
	
	/**
	 * 
	 * 
	 * All the methods for adding info to files
	 * 
	 */
	
	public void setString(Player p, String path, String value)
	{
		File pFile = new File(usersDir + File.separator + p.getName() + ".yml");
		this.config = YamlConfiguration.loadConfiguration(pFile);
		
		this.config.set(path, value);
	
		try
		{
			this.config.save(pFile);
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	public void setInt(Player p, String path, int value)
	{
		File pFile = new File(usersDir + File.separator + p.getName() + ".yml");
		this.config = YamlConfiguration.loadConfiguration(pFile);
		
		this.config.set(path, value);
	
		try
		{
			this.config.save(pFile);
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	public void setBoolean(Player p, String path, boolean value)
	{
		File pFile = new File(usersDir + File.separator + p.getName() + ".yml");
		this.config = YamlConfiguration.loadConfiguration(pFile);
		
		this.config.set(path, value);
	
		try
		{
			this.config.save(pFile);
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	public void setStringToUuidFile(Player p, String path, String value)
	{
		this.config = YamlConfiguration.loadConfiguration(uuidEntry);
		
		this.config.set(path, value);
	}
	
	
	/**
	 * 
	 * 
	 * All the methods for retrieving player info
	 * 
	 */
	
	public String getString(Player p, String path)
	{
		File pFile = new File(usersDir + File.separator + p.getName() + ".yml");
		this.config = YamlConfiguration.loadConfiguration(pFile);
		
		return (String) this.config.get(path);
	}
	
	public boolean getBoolean(Player p, String path)
	{
		File pFile = new File(usersDir + File.separator + p.getName() + ".yml");
		this.config = YamlConfiguration.loadConfiguration(pFile);
		
		return (boolean) this.config.get(path);
	}
	
	public int getInt(Player p, String path)
	{
		File pFile = new File(usersDir + File.separator + p.getName() + ".yml");
		this.config = YamlConfiguration.loadConfiguration(pFile);
		
		return (int) this.config.get(path);
	}
	
	public String getStringFromUuidEntry(String path)
	{
		this.config = YamlConfiguration.loadConfiguration(uuidEntry);
		
		return (String) this.config.get(path);
	}
	
	public void renamePlayerFile(Player p, String newName)
	{
		File pFile = new File(usersDir + File.separator + p.getName() + ".yml");
		File newFile = new File(usersDir + File.separator + newName + ".yml");
		
		pFile.renameTo(newFile);
	}
	
	
	public void saveConfig()
	{
		for(int i = 0; i < Bukkit.getServer().getOnlinePlayers().length; i++)
		{
			Player[] players = Bukkit.getServer().getOnlinePlayers();
			File pFile = new File(usersDir + File.separator + players[i].getName() + ".yml");
			this.config = YamlConfiguration.loadConfiguration(pFile);
			
			try
			{
				this.config.save(pFile);
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
		}
		
		this.config = YamlConfiguration.loadConfiguration(uuidEntry);
		
		try
		{
			this.config.save(uuidEntry);
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		
		Bukkit.getServer().broadcastMessage(ChatColor.GREEN + "FCR Config Saved");
	}
	
	public void loadConfig()
	{
		for(int i = 0; i < Bukkit.getServer().getOnlinePlayers().length; i++)
		{
			Player[] players = Bukkit.getServer().getOnlinePlayers();
			File pFile = new File(usersDir + File.separator + players[i].getName() + ".yml");
			this.config = YamlConfiguration.loadConfiguration(pFile);
			
			try
			{
				this.config.load(pFile);
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
		}
		
		this.config = YamlConfiguration.loadConfiguration(uuidEntry);
		
		try
		{
			this.config.load(uuidEntry);
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		
		Bukkit.getServer().broadcastMessage(ChatColor.GREEN + "FCR Config Loaded");
	}
}