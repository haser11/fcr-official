package com.fallencraftmc.Main.Players;

import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;
import org.bukkit.potion.PotionEffectType;

import com.fallencraftmc.Main.BasicInfo;
import com.fallencraftmc.Main.FCUtil;
import com.fallencraftmc.Main.Config.ConfigManager;
import com.fallencraftmc.Main.World.CustomNPC;

public class CommandManager implements CommandExecutor
{
	Plugin plugin;
	
	public CommandManager(Plugin p)
	{
		this.plugin = p;
	}
	
	ConfigManager cm = new ConfigManager(plugin);
	PlayerDeathManager pdm = new PlayerDeathManager(plugin);

	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) 
	{
		Player p = (Player) sender;
		if(command.getName().equalsIgnoreCase("fcr"))
		{
			if(args.length == 0)
			{
				p.sendMessage(ChatColor.BLUE + "[FCR] " + ChatColor.DARK_AQUA + "FCR (FCRoleplay) is a custom RPG plugin being developed and maintained by"
						+ " haser11.");
				p.sendMessage(ChatColor.RED + "Available commands: ");
				p.sendMessage(ChatColor.RED + "/fcr setclass USERNAME CLASSNAME");
				p.sendMessage(ChatColor.RED + "/fcr classname");
				p.sendMessage(ChatColor.RED + "/fcr saveconfig");
				p.sendMessage(ChatColor.RED + "/fcr loadconfig");
				p.sendMessage(ChatColor.RED + "/fcr comeback");
				return true;

			}
			else if(args.length == 1)
			{
				if(args[0].equalsIgnoreCase(BasicInfo.SAVECONFIG))
				{
					cm.saveConfig();
					return true;
				}
				
				else if(args[0].equalsIgnoreCase(BasicInfo.LOADCONFIG))
				{
					cm.loadConfig();
					return true;
				}
				
				else if(args[0].equalsIgnoreCase(BasicInfo.ENDTIMER))
				{
					cm.setBoolean(p, BasicInfo.ISDEAD, false);
					DeathTimer dm = new DeathTimer(plugin, p);
					
					dm.taskId = 30;
					p.sendMessage(ChatColor.GREEN + "You have successfully overriden the death timer!");
					cm.setBoolean(p, BasicInfo.ISDEAD, false);
					p.setHealth(20.0);
					p.setFoodLevel(20);
					p.removePotionEffect(PotionEffectType.SLOW);
					p.removePotionEffect(PotionEffectType.BLINDNESS);
					return true;

				}
				else if(args[0].equalsIgnoreCase(BasicInfo.CLASSLIST))
				{
					p.sendMessage(ChatColor.BLUE + "List of available classes: ");
					p.sendMessage(ChatColor.WHITE + "MEDIC, SOLDIER, SCOUT, THEIF, ARCHER, WARRIOR, FARMER, MINER, BLACKSMITH");
					return true;

				}
				else if(args[0].equalsIgnoreCase("kill") && pdm.isFakeDead(p))
				{
					pdm.killPlayer(p);
				}
				else
				{
					return false;
				}
			}
			else if(args.length == 2)
			{
				if(args[0].equalsIgnoreCase(BasicInfo.GETUUID))
				{
					String target = args[1];
					p.sendMessage(ChatColor.GRAY + "The UUID of " + target + " is: " + Bukkit.getOfflinePlayer(target).getUniqueId());
					return true;
				}
				else if(args[0].equalsIgnoreCase(BasicInfo.GETNAME))
				{
					p.sendMessage(ChatColor.GRAY + "This is not yet working");
					return true;
				}
			}
			else if(args.length == 3)
			{
				if(args[0].equalsIgnoreCase(BasicInfo.SETCLASS))
				{
				String pName = args[1];
				Player target = Bukkit.getServer().getPlayer(pName);
				
				if(args[2].equalsIgnoreCase("medic"))
					new PlayerClassSystem(target, "medic"); cm.setString(target, BasicInfo.CLASSINFO, "medic");
				if(args[2].equalsIgnoreCase("soldier"))
					new PlayerClassSystem(target, "soldier"); cm.setString(target, BasicInfo.CLASSINFO, "soldier");
				if(args[2].equalsIgnoreCase("scout"))
					new PlayerClassSystem(target, "scout"); cm.setString(target, BasicInfo.CLASSINFO, "scout");
				if(args[2].equalsIgnoreCase("theif"))
					new PlayerClassSystem(target, "theif"); cm.setString(target, BasicInfo.CLASSINFO, "theif");
				if(args[2].equalsIgnoreCase("archer"))
					new PlayerClassSystem(target, "archer"); cm.setString(target, BasicInfo.CLASSINFO, "archer");
				if(args[2].equalsIgnoreCase("warrior"))
					new PlayerClassSystem(target, "warrior"); cm.setString(target, BasicInfo.CLASSINFO, "warrior");
				if(args[2].equalsIgnoreCase("farmer"))
					new PlayerClassSystem(target, "farmer"); cm.setString(target, BasicInfo.CLASSINFO, "farmer");
				if(args[2].equalsIgnoreCase("miner"))
					new PlayerClassSystem(target, "miner"); cm.setString(target, BasicInfo.CLASSINFO, "miner");
				if(args[2].equalsIgnoreCase("blacksmith"))
					new PlayerClassSystem(target, "blacksmith"); cm.setString(p, BasicInfo.CLASSINFO, "blacksmith");
					
					return true;
				}
				else if(args[0].equalsIgnoreCase(BasicInfo.SPWANMOB))
				{
					CustomNPC cmp = new CustomNPC(p.getLocation(), FCUtil.stringToEntityType(args[1]), args[2], true);
					p.sendMessage(ChatColor.GREEN + "Successfully spawned " + args[1].toUpperCase());
					return true;

				}
				else
				{
					return false;
				}
			}
			else
			{
				return false;
			}
		}
		return false;
	}
}