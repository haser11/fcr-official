package com.fallencraftmc.Main.Players;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerLoginEvent;
import org.bukkit.plugin.Plugin;

import com.fallencraftmc.Main.Config.ConfigManager;

public class PlayerLoginManager implements Listener
{
	Plugin plugin;
	ConfigManager cm = new ConfigManager(plugin);
	public PlayerLoginManager(Plugin p)
	{
		this.plugin = p;
	}
	
	@EventHandler
	public void onLogin(PlayerLoginEvent event)
	{
		if(cm.checkIfNewPlayer(event.getPlayer()))
		{
		Player p = event.getPlayer();
		cm.addPlayerToUuidRegistry(p);
		cm.createPlayerConfigFile(event.getPlayer());
		cm.addDefaultPlayerConfig(p);
		}
		
		if(cm.getStringFromUuidEntry("Players." + event.getPlayer().getUniqueId()) != event.getPlayer().getName())
		{
			cm.setStringToUuidFile(event.getPlayer(), "Players." + event.getPlayer().getUniqueId(), event.getPlayer().getName());
			cm.renamePlayerFile(event.getPlayer(), event.getPlayer().getName());
		}
	}
}
