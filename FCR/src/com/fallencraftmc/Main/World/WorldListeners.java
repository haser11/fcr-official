package com.fallencraftmc.Main.World;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.plugin.Plugin;

import com.fallencraftmc.Main.BasicInfo;
import com.fallencraftmc.Main.Config.ConfigManager;
import com.sk89q.worldguard.bukkit.WGBukkit;
import com.sk89q.worldguard.protection.managers.RegionManager;
import com.sk89q.worldguard.protection.regions.ProtectedCuboidRegion;

public class WorldListeners implements Listener
{
	Plugin plugin;
	public WorldListeners(Plugin p)
	{
		this.plugin = p;
	}
	
	ConfigManager cm = new ConfigManager(plugin);
	
	//Miner
	@EventHandler
	public void onMine(BlockBreakEvent event)
	{
		if(!cm.getString(event.getPlayer(), "Info.class").equalsIgnoreCase("miner")  && 
				!event.getPlayer().isOp() && !event.getPlayer().hasPermission(BasicInfo.MINEPERM))
		{
			if(event.getBlock().getType().equals(Material.COAL_ORE))
			{
				event.setCancelled(true); 
				event.getPlayer().sendMessage(ChatColor.RED + "You don't know how to mine...");
			}	
			else if(event.getBlock().getType().equals(Material.IRON_ORE))
			{
				event.setCancelled(true); 
				event.getPlayer().sendMessage(ChatColor.RED + "You don't know how to mine...");
			}
			else if(event.getBlock().getType().equals(Material.GOLD_ORE))
			{
				event.setCancelled(true); 
				event.getPlayer().sendMessage(ChatColor.RED + "You don't know how to mine...");
			}
			else if(event.getBlock().getType().equals(Material.REDSTONE_ORE) || event.getBlock().getType().equals(Material.GLOWING_REDSTONE_ORE))
			{
				event.setCancelled(true); 
				event.getPlayer().sendMessage(ChatColor.RED + "You don't know how to mine...");
			}
			else if(event.getBlock().getType().equals(Material.LAPIS_ORE))
			{
				event.setCancelled(true); 
				event.getPlayer().sendMessage(ChatColor.RED + "You don't know how to mine...");
			}
			else if(event.getBlock().getType().equals(Material.DIAMOND_ORE))
			{
				event.setCancelled(true); 
				event.getPlayer().sendMessage(ChatColor.RED + "You don't know how to mine...");
			}
		}
			
	}
	
	//Farmer
	@EventHandler
	public void onTill(PlayerInteractEvent event)
	{
		if(event.getAction().equals(Action.RIGHT_CLICK_BLOCK))
		{
			if(!cm.getString(event.getPlayer(), "Info.class").equalsIgnoreCase("farmer") && 
					!event.getPlayer().isOp() && !event.getPlayer().hasPermission(BasicInfo.FARMPERM))
			{
				Player p = event.getPlayer();
				
				if(p.getItemInHand().getType().equals(Material.WOOD_HOE))
				{
					event.setCancelled(true); 
					p.sendMessage(ChatColor.RED + "You don't know how to till...");
				}
				else if(p.getItemInHand().getType().equals(Material.STONE_HOE))
				{
					event.setCancelled(true); 
					p.sendMessage(ChatColor.RED + "You don't know how to till...");
				}
				else if(p.getItemInHand().getType().equals(Material.IRON_HOE))
				{
					event.setCancelled(true); 
					p.sendMessage(ChatColor.RED + "You don't know how to till...");
				}
				else if(p.getItemInHand().getType().equals(Material.GOLD_HOE))
				{
					event.setCancelled(true); 
					p.sendMessage(ChatColor.RED + "You don't know how to till...");
				}
				else if(p.getItemInHand().getType().equals(Material.DIAMOND_HOE))
				{
					event.setCancelled(true); 
					p.sendMessage(ChatColor.RED + "You don't know how to till...");
				}
			}
		}
	}
	
	@EventHandler
	public void onPlant(PlayerInteractEvent event)
	{
		if(event.getAction().equals(Action.RIGHT_CLICK_BLOCK))
		{
			if(!cm.getString(event.getPlayer(), "Info.class").equalsIgnoreCase("farmer") && !event.getPlayer().isOp() && 
					!event.getPlayer().hasPermission(BasicInfo.FARMPERM))
			{
				Player p = event.getPlayer();
				
				if(p.getItemInHand().getType().equals(Material.SEEDS))
				{
					event.setCancelled(true);
					p.sendMessage(ChatColor.RED + "You don't know how to plant...");
				}
				else if(p.getItemInHand().getType().equals(Material.POTATO) || p.getItemInHand().getType().equals(Material.POTATO_ITEM))
				{
					event.setCancelled(true);
					p.sendMessage(ChatColor.RED + "You don't know how to plant...");
				}
				else if(p.getItemInHand().getType().equals(Material.CARROT) || p.getItemInHand().getType().equals(Material.CARROT_ITEM))
				{
					event.setCancelled(true);
					p.sendMessage(ChatColor.RED + "You don't know how to plant...");
				}
				else if(p.getItemInHand().getType().equals(Material.PUMPKIN_SEEDS))
				{
					event.setCancelled(true);
					p.sendMessage(ChatColor.RED + "You don't know how to plant...");
				}
				else if(p.getItemInHand().getType().equals(Material.MELON_SEEDS))
				{
					event.setCancelled(true);
					p.sendMessage(ChatColor.RED + "You don't know how to plant...");
				}
			}
		}
	}
	
	public com.sk89q.worldedit.BlockVector convertToSk89qBV(Location location)
	{
		return new com.sk89q.worldedit.BlockVector(location.getX(),location.getY(),location.getZ());
	}
	
	//Town claim 
	@EventHandler
	public void onBreak(BlockBreakEvent event)
	{
		if(event.getBlock().getType().equals(Material.DRAGON_EGG) && !event.getPlayer().isOp())
		{
			event.setCancelled(true);
			event.getBlock().setType(Material.AIR);
			Location blockLoc = event.getBlock().getLocation();
			Location point1 = new Location(event.getBlock().getWorld(), blockLoc.getX() + 150.0, blockLoc.getY() + 500, blockLoc.getZ() + 150.0);	
			Location point2 = new Location(event.getBlock().getWorld(), blockLoc.getX() - 150.0, blockLoc.getY() - 500, blockLoc.getZ() - 150.0);	
			
			ProtectedCuboidRegion newRegion = new ProtectedCuboidRegion(event.getPlayer().getName() + "Town", this.convertToSk89qBV(point1)
					, this.convertToSk89qBV(point2));
			
			WGBukkit.getRegionManager(event.getBlock().getWorld()).addRegion(newRegion);
			
			Bukkit.getServer().broadcastMessage(ChatColor.GREEN + "Player " + event.getPlayer().getName() + " has conquered a new city!" + " This is a test"
					+ "for Haser" + newRegion.getId() + " " + event.getPlayer().getUniqueId());
		}
	}
	
	
}
